# coding:utf-8
import pysrt
import os
import re

htmltagregex = re.compile(r'<[^>]+>',re.S)
for f in os.listdir('data/'):
    if f.endswith('.srt'):
        f = 'data/' + f
        subs = pysrt.open(f, encoding='iso-8859-1')
        for s in subs:
            s = str(s)
            s = htmltagregex.sub('', s)
            s = s.replace('-', '')
            s = s.split("\n")
            s = s[2:-1]
            s = " ".join(s)
            print(s)
